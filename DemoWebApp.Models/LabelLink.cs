﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoWebApp.Models
{
    /// <summary>
    /// Creates a many-to-many relationship table which links Note entities to Label entities
    /// </summary>
    public class LabelLink
    {
        public int NoteId { get; set; }

        public Note Note { get; set; }

        public int LabelId { get; set; }

        public Label Label { get; set; }        
    }
}
