﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace DemoWebApp.Models
{
    public class Project
    {        
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
