﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DemoWebApp.Models
{
    public class Label
    {        
        [Key]
        public int Id { get; set; }

        [Required]
        public string Kind { get; set; }

        // Used for many-to-many relationship between Note and Label
        public List<LabelLink> LabelLinks { get; set; }

        // Gets or sets info as to whether this label is currently selected in the GUI
        [NotMapped]
        public bool IsSelected { get; set; }
    }
}
