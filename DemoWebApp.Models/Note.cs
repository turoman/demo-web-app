﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.InteropServices;

namespace DemoWebApp.Models
{
    public class Note
    {        
        [Key]
        public int Id { get; set; }
        
        [Required]
        public string Title { get; set; }

        [Required]
        public int ProjectId { get; set; }

        public Project Project { get; set; }

        public string Description { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }
               
        public DateTime? UpdatedAt { get; set; }
               
        [NotMapped]
        public List<Int32> SelectedLabels { get; set; }

        // Used for many-to-many relationship between Note and Label
        public ICollection<LabelLink> LabelLinks { get; set; }
        
    }
}
