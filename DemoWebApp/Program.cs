using DemoWebApp;
using DemoWebApp.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;

namespace DemoWebApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                var host = CreateHostBuilder(args).Build();

                if (args[0] == "setup")
                {
                    Console.WriteLine("Running the application with initial setup");
                    using (var scope = host.Services.CreateScope())
                    {
                        var services = scope.ServiceProvider;

                        try
                        {
                            var config = host.Services.GetRequiredService<IConfiguration>();
                            SeedData.Initialize(services, config["AdminPass"], config["UserPass"]).Wait();
                        }
                        catch (Exception ex)
                        {
                            var logger = services.GetRequiredService<ILogger<Program>>();
                            logger.LogError(ex, "An error occurred seeding the DB.");
                        }
                    }                   
                }
                else
                {
                    Console.WriteLine("Running the application in standard mode (no initial setup)");
                }

                host.Run();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        public static IHostBuilder CreateHostBuilder(string[] args) 
            =>Host.CreateDefaultBuilder(args).ConfigureWebHostDefaults(
                webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                }
            );
    }
}
