﻿using DemoWebApp.DataAccess.Data;
using DemoWebApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoWebApp.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrator")]
    public class NotesController : Controller
    {
        private readonly DataAccess.Data.DemoAppContext _context;

        public NotesController(DataAccess.Data.DemoAppContext context)
        {
            _context = context;
        }

        public List<Label> PopulateLabels()
        {
            var allLabels = _context.Labels.OrderBy(k => k.Kind);

            List<Label> listOfLabels = new List<Label>();
            foreach (Label chg in allLabels)
            {
                listOfLabels.Add(new Label
                {
                    Id = chg.Id,
                    Kind = chg.Kind
                });
            }

            return listOfLabels;
        }

        public List<Label> PopulateLabelsForEdit(Note note)
        {
            var allLabels = _context.Labels.OrderBy(k => k.Kind);
            var selectedLabels = new HashSet<int>(note.LabelLinks.Select(c => c.LabelId));

            List<Label> listOfLabels = new List<Label>();
            foreach (Label label in allLabels)
            {
                listOfLabels.Add(new Label
                {
                    Id = label.Id,
                    Kind = label.Kind,
                    IsSelected = selectedLabels.Contains(label.Id)
                });
            }

            return listOfLabels;
        }

        // GET: Notes
        public async Task<IActionResult> Index(string sortOrder, int? pageNumber)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["ProjectSort"] = sortOrder == "Project" ? "project_desc" : "Project";
            ViewData["DateSort"] = sortOrder == "Date" ? "date_desc" : "Date";

            IQueryable<Note> notes = _context.Notes.Include(ch => ch.Project)
                .Include(c => c.LabelLinks).ThenInclude(t => t.Label);

            switch (sortOrder)
            {
                case "Project":
                    notes = notes.OrderBy(ch => ch.Project);
                    break;
                case "project_desc":
                    notes = notes.OrderByDescending(ch => ch.Project);
                    break;
                case "Date":
                    notes = notes.OrderBy(ch => ch.CreatedAt);
                    break;
                case "date_desc":
                    notes = notes.OrderByDescending(ch => ch.CreatedAt);
                    break;
                default:
                    notes = notes.OrderByDescending(ch => ch.CreatedAt);
                    break;
            }

            int pageSize = 5;
            return View(await PaginatedList<Note>.CreateAsync(notes.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        // GET: Notes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var note = await _context.Notes
                .Include(d => d.Project)
                .Include(c => c.LabelLinks)
                .ThenInclude(t => t.Label)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (note == null)
            {
                return NotFound();
            }

            return View(note);
        }

        // GET: Notes/Create
        public IActionResult Create()
        {
            ViewData["ProjectId"] = new SelectList(_context.Projects.OrderBy(p => p.Name), "Id", "Name");
            ViewData["Labels"] = PopulateLabels();
            return View();
        }


        // POST: Notes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Title,ProjectId,Description,SelectedLabels")] Note note)
        {
            if (ModelState.IsValid)
            {
                if (note.SelectedLabels != null)
                {
                    note.LabelLinks = new List<LabelLink>();
                    foreach (var selectedID in note.SelectedLabels)
                    {
                        var changeToAdd = new LabelLink
                        {
                            LabelId = selectedID,
                        };
                        note.LabelLinks.Add(changeToAdd);
                    }
                }

                note.CreatedAt = DateTime.Now;

                if (await TryUpdateModelAsync<Note>(
                note,
                "Note",
                i => i.Title, i => i.ProjectId, i => i.Description, i => i.CreatedAt))
                {
                    _context.Notes.Add(note);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    return View(note);
                }
            }
            else
            {
                ModelState.AddModelError("", "Error saving the record");
                ViewData["ProjectId"] = new SelectList(_context.Projects, "Id", "Name", note.ProjectId);
                return View(note);
            }
        }

        // GET: Notes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            // Get the note with this ID AND include the label links as well
            var note = await _context.Notes
                .Include(c => c.LabelLinks)
                .AsNoTracking()
                .FirstOrDefaultAsync(c => c.Id == id);

            if (note == null)
            {
                return NotFound();
            }

            ViewData["ProjectId"] = new SelectList(_context.Projects.OrderBy(p => p.Name), "Id", "Name", note.ProjectId);
            ViewData["Labels"] = PopulateLabelsForEdit(note);
            return View(note);
        }

        // POST: Notes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,ProjectId,Description,SelectedLabels")] Note note)
        {
            if (id != note.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var recordToUpdate = await _context.Notes
                    .Include(c => c.LabelLinks)
                    .FirstOrDefaultAsync(c => c.Id == id);

                    // Set the "update at" date                    
                    recordToUpdate.UpdatedAt = DateTime.Now;

                    // Set the LabelLinks for this object
                    UpdateLinkedLabels(recordToUpdate, note.SelectedLabels);

                    if (await TryUpdateModelAsync<Note>(
                    recordToUpdate,
                    "",
                    i => i.Title, i => i.ProjectId, i => i.Description))
                    {
                        _context.Notes.Update(recordToUpdate);
                        await _context.SaveChangesAsync();
                        return RedirectToAction(nameof(Index));
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NoteExists(note.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            ViewData["ProjectId"] = new SelectList(_context.Projects, "Id", "Name", note.ProjectId);
            return View(note);
        }

        public void UpdateLinkedLabels(Note note, List<int> selectedLabels)
        {
            foreach (LabelLink item in note.LabelLinks)
            {
                _context.Remove(item);
            }

            if (selectedLabels != null)
            {
                foreach (int item in selectedLabels)
                {
                    note.LabelLinks.Add(new LabelLink { LabelId = item });
                }
            }
        }

        // GET: Notes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var note = await _context.Notes
                .Include(d => d.Project)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (note == null)
            {
                return NotFound();
            }

            return View(note);
        }

        // POST: Notes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var note = await _context.Notes.FindAsync(id);
            _context.Notes.Remove(note);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool NoteExists(int id)
        {
            return _context.Notes.Any(e => e.Id == id);
        }
    }
}
