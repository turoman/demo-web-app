using ContactManager.Authorization;
using DemoWebApp.Authorization;
using DemoWebApp.DataAccess.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;

namespace DemoWebApp
{
    public class Startup
    {        
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {      
            // Add MVC controllers
            services.AddControllersWithViews().AddRazorRuntimeCompilation();            

            // Add database            
            services.AddDbContext<DataAccess.Data.DemoAppContext>(opt => opt.UseSqlite(Configuration.GetConnectionString("conSqlite"), b => b.MigrationsAssembly("DemoWebApp.DataAccess")));

            // Add Identity
            services.AddDefaultIdentity<IdentityUser>(
                options => options.SignIn.RequireConfirmedAccount = true)
                .AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<DataAccess.Data.DemoAppContext>();
                       
            // Add Authorization Handlers
            services.AddSingleton<IAuthorizationHandler, AdministratorsAuthorizationHandler>();

            // The policy is used to hide certain parts in views based on role
            services.AddAuthorization(options =>
            {
                options.AddPolicy("RequireAdministratorRole", policy => policy.RequireRole(Constants.AdministratorsRole));
            });

            }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
                        
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseRouting();            
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{area=Public}/{controller=Home}/{action=Index}/{id?}");                
                endpoints.MapRazorPages();
            });
        }
    }
}
