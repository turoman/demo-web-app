# README

**DemoWebApp** is a Web application developed in ASP.NET Core with Entity Framework Core. The source code includes a SQlite database with some initial data, including two user accounts. To *run* the app in the current state, just press the debug button in Visual Studio.

The rest of the instructions are only applicable if you would like to build the app from scratch. Otherwise, you can safely skip them.

## Prerequisites

To build and run the app, you must either have the Entity Framework Core CLI tools installed, or use the Package Manager Console directly from Visual Studio. This help assumes that you are using the Package Manager Console in Visual Studio. You can open this window from **View > Other Windows > Package Manager Console**.

## Configure users

This app requires user authentication. There are two roles: standard users and administrators. Since passwords are not meant to be saved inside source code, you can set the user passwords as follows:

1. In the Solution Explorer window of Visual Studio, right click the **DemoWebApp** project and select **Manage User Secrets** from the context menu.
2. Paste or add the passwords of your choice to the .json file, for example:

    ```
    {
      "AdminPass": "Admin123!",
      "UserPass": "User123!"
    }
    ```

The database will be populated with these two accounts when you run the application in **setup** mode, as further described below.

## Add initial migration

The source code already includes an initial migration, so you don't normally need this step. Only if you have deleted the existing migration and would like to create a new initial migration for whatever reason, do the following:

1. In the Package Manager Console window, select *DemoWebApp.DataAcces* as default project.
2. Run:

   ```
   Add-Migration Initial
   ```

## Create and populate database

The source code already includes a database, so you don't normally need this step. Only if you would like to create the database again from the latest migration, do the following:

1. Make sure that you have at least one migration (for example, the initial migration, as described above).
2. In the Package Manager Console window, select *DemoWebApp.DataAcces* as default project, and run:

```
Update-Database
```

This creates a new SQLite database in the Data subdirectory of the **DemoWebApp** project.

## Running the application

The application has two run modes:

1. Initial run (with setup) - The application should be run in this mode only once, to populate the database. This mode adds the database users and a few records to the database.
2. Standard run - this just runs the application and assumes the users and some data already exist in the database. 

To run the application in setup or standard mode:

1. Right-click the **DemoWebApp** project in Solution Explorer, and select **Properties**.
2. Click the **Debug** tab.
3. In the **Application arguments** text box, enter `setup`.
4. Run the application. Make sure that you run the profile where you changed the arguments (for example, "IIS Express"). After running the application, you should be able to log in as an admin or standard user following the instructions on the home page. You should also see some initial data (a note, two labels, two projects).
5. Go back to the **Application arguments** text box and change the `setup` argument to something else, for example, to `0`. The application will run in standard mode from now on.



