﻿using DemoWebApp.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using DemoWebApp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoWebApp.DataAccess.Data
{
    public class DemoAppContext : IdentityDbContext
    {
        public DemoAppContext(DbContextOptions<DemoAppContext> options) : base(options)
        {
               
        }

        public DbSet<Note> Notes { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Label> Labels { get; set; }
        public DbSet<LabelLink> LabelLinks { get; set; }

        protected override void OnModelCreating (ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Composite key that ensures that the same combination of 
            // NoteId and LabelId cannot exist multiple times in the LabelLinks table
            modelBuilder.Entity<LabelLink>().HasKey(ll => new { ll.NoteId, ll.LabelId });

            // Many to many relationship
            modelBuilder.Entity<LabelLink>()
                .HasOne(l => l.Label)
                .WithMany(ll => ll.LabelLinks)
                .HasForeignKey(ct => ct.LabelId);

            modelBuilder.Entity<LabelLink>()
                .HasOne(l => l.Note)
                .WithMany(ll => ll.LabelLinks)
                .HasForeignKey(n => n.NoteId);
         }
    }
}
