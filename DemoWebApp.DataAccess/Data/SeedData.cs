﻿using DemoWebApp.DataAccess.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DemoWebApp.Authorization;

namespace DemoWebApp.Models
{
    public class SeedData
    {
        public static async Task Initialize(IServiceProvider serviceProvider, string adminPass, string userPass)
        {
            using (var context = new DataAccess.Data.DemoAppContext(serviceProvider.GetRequiredService<DbContextOptions<DataAccess.Data.DemoAppContext>>()))
            {
                // Add the admin user to the database.
                // Password is set with the following:
                // dotnet user-secrets set adminPass <pw>
                var adminID = await EnsureUser(serviceProvider, adminPass, "admin@example.org");
                await EnsureRole(serviceProvider, adminID, Constants.AdministratorsRole);

                // Add a standard user
                var userID = await EnsureUser(serviceProvider, userPass, "user@example.org");
                await EnsureRole(serviceProvider, userID, Constants.UsersRole);

                SeedDB(context);
            }
        }

        private static async Task<string> EnsureUser(IServiceProvider serviceProvider,
                                            string testUserPw, string UserName)
        {
            var userManager = serviceProvider.GetService<UserManager<IdentityUser>>();

            var user = await userManager.FindByNameAsync(UserName);
            if (user == null)
            {
                user = new IdentityUser
                {
                    UserName = UserName,
                    EmailConfirmed = true
                };
                await userManager.CreateAsync(user, testUserPw);
            }

            if (user == null)
            {
                throw new Exception("The password is probably not strong enough!");
            }

            return user.Id;
        }

        private static async Task<IdentityResult> EnsureRole(IServiceProvider serviceProvider,
                                                                      string uid, string role)
        {
            IdentityResult IR = null;
            var roleManager = serviceProvider.GetService<RoleManager<IdentityRole>>();

            if (roleManager == null)
            {
                throw new Exception("roleManager null");
            }

            if (!await roleManager.RoleExistsAsync(role))
            {
                IR = await roleManager.CreateAsync(new IdentityRole(role));
            }

            var userManager = serviceProvider.GetService<UserManager<IdentityUser>>();

            var user = await userManager.FindByIdAsync(uid);

            if (user == null)
            {
                throw new Exception("The testUserPw password was probably not strong enough!");
            }

            IR = await userManager.AddToRoleAsync(user, role);

            return IR;
        }

        public static void SeedDB(DataAccess.Data.DemoAppContext context)
        {
            Project proj1 = new Project { Id = 1, Name = "Home" };            
            context.Projects.Add(proj1);
            Project proj2 = new Project { Id = 2, Name = "Garden" };
            context.Projects.Add(proj2);

            Label label1 = new Label { Id = 1, Kind = "To do" };
            context.Labels.Add(label1);
            Label label2 = new Label { Id = 2, Kind = "Done" };
            context.Labels.Add(label2);

            Note note1 = new Note
            {
                CreatedAt = DateTime.Now,
                Title = "Lorem ipsum",
                ProjectId = 2,
                Description = "Per aspera ad astra",
            };

            List<LabelLink> labelLinks = new List<LabelLink>();
            LabelLink link1 = new LabelLink { LabelId = 1, NoteId = 1 };
            LabelLink link2 = new LabelLink { LabelId = 2, NoteId = 1 };
            labelLinks.Add(link1);
            labelLinks.Add(link2);
            note1.LabelLinks = labelLinks;
            context.Notes.Add(note1);

            context.SaveChanges();

        }

    }
}
